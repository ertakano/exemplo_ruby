# language:pt

Funcionalidade: Começar jogo
    Para poder passar o tempo
    Sendo um jogador
    Quero poder começar um jogo novo

Cenario: Começo de novo jogo com sucesso
    Ao começar o jogo, é mostrado a mensagem 
    inicial para o jogador

    Quando começo um jogo novo
    Então vejo a seguinte mensagem na tela:
    """
    Bem vindo ao jogo da forca!
    """

    
