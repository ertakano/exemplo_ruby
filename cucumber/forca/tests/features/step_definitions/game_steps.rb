#encoding: utf-8
require_relative '../../../src/game.rb'


  Quando("começo um jogo novo") do
 
    @game = Game.new
    @game.start

  end
  
  Então("vejo a seguinte mensagem na tela:") do |message|
    
    expect(@game.output).to eql message

  end