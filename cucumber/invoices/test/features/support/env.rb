require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rspec'
require 'site_prism'
require 'faker'
require 'ostruct'

require_relative 'helpers'
require_relative 'page_objects'

World(Helpers)
World(PageObjects)

$env = ENV['ENV']

$data = YAML.load_file(File.dirname(__FILE__) + "/data/#{$env}.yaml")

Faker::Config.locale = 'pt-BR'

Capybara.configure do |config|
    config.default_driver = :selenium
    config.app_host = $data['url']
end

Capybara.default_max_wait_time = 5


