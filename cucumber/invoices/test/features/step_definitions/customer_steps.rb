#encoding: utf-8

Dado("acessa o cadastro de clientes") do
    login.load
    login.do_login('kato.danzo@qaninja.com.br', 'secret')
    dash.wait_for_title
    customer.load
end
  
Dado("que eu tenha um novo cliente") do

	@new_customer = get_fake_customer
end
  
Quando("faço o cadastro do cliente") do
    customer.save(@new_customer)
end
  
Então("esse cliente deve ser exibido na busca") do
    customer.search_field.set @new_customer.email
    customer.search_button.click

    expect(customer.view.size).to eql 1
    expect(customer.view.first.text).to have_content @new_customer.name
    expect(customer.view.first.text).to have_content @new_customer.email
    expect(customer.view.first.text).to have_content @new_customer.phone
end 
  
E("excluir o cliente") do
	customer.delete_link.click
	customer.delete_confirm.click
end

Dado("que eu tenho um novo cliente com os atributos:") do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  pending # Write code here that turns the phrase above into concrete actions
end

Dado("esse cliente não deve estar cadastrado no sistema") do
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("faço o cadastro desse cliente") do
  pending # Write code here that turns the phrase above into concrete actions
end