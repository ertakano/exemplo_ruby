#encoding:utf-8

Dado ("que eu realize login") do
    login.load
end

Dado("que eu tenha um usuario {string} com os atributos:") do |user_profile|
    
    user = $data['users'][user_profile]
    @email = user['email']
    @senha = user['password']
end
  
Quando("faço login") do
    login.do_login(@email, @senha)
end
  
Então("vejo o Dashoboard com a mensagem {string}") do |message|

    expect(dash.title.text).to eql message
    expect(dash.nav.user_menu.text).to eql @email
end

Dado("que eu tenha uma lista com os seguintes usuarios:") do |table|
    @users = table.hashes
end
  
Quando("faço uma tentativa de login") do
    
    @messages_error = Array.new
    @messages_spec  = Array.new

    login.do_login_vazio

    @users.each do |user|
        login.do_login(user['email'], user['password'])

    @messages_error.push(login.msg_errors.text)
    @messages_spec.push(user['message'])
    end
end
  
Então("vejo uma mensagem de erro na página de login") do

    expect(@messages_error).to eql @messages_spec

end 