#language: pt

Funcionalidade: Cadastro de Clientes
    Sendo um usuário cadastrado
    Posso acessar o cadastro de clientes
    Para fins de gerenciamento e também atendimento dos meus clientes

Contexto: Realiza Login
    * que eu realize login
    * acessa o cadastro de clientes
    
@cadastro @logout
Cenário: Cadastrar novo cliente

    Dado que eu tenha um novo cliente
    Quando faço o cadastro do cliente
    Então esse cliente deve ser exibido na busca
    E excluir o cliente

Cenário: Cadastrar novo cliente com dados estáticos

    Nesse exemplo vamos excluir o cliente do banco de dados
    Usando Data Table + MongoDB

    Dado que eu tenho um novo cliente com os atributos:
    |name|Nome Exemplo|
    |phone|11 837737733|
    |email|teste@teste16102016.com.br|
    |gender|M|
    |type|Gold|
    |notes|Lorem Ipsun|
    E esse cliente não deve estar cadastrado no sistema
    Quando faço o cadastro desse cliente
    Então esse cliente deve ser exibido na busca
    