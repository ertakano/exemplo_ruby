#language:pt

Funcionalidade: 
    Realizar login como administrador

Contexto: Realiza Login
Dado que eu realize login

@smoke @logout
Cenário: Administrador faz login

    Dado que eu tenha um usuario "admin" com os atributos:
    Quando faço login
    Então vejo o Dashoboard com a mensagem "Dashboard"

@smoke @logout
Cenário: Tentativa de login

    Dado que eu tenha uma lista com os seguintes usuarios:
    | email                        | password | message                  |
    | kato.danzo@qaninja.com.br    | 123456   | Senha inválida.          |
    | padre.quevedo@qaninja.com.br | secret   | Usuário não cadastrado.  |
    | kato.danzo&qaninja.com.br    | secret   | Informe um email válido. |
    Quando faço uma tentativa de login
    Então vejo uma mensagem de erro na página de login
