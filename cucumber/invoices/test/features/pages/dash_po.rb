require_relative 'sections'


class DashPage <SitePrism::Page
	section :nav, Sections::NavBar, '#navbar'

    element :title, '#title_row #page_title'
end


