require_relative 'sections'

class CustomerPage <SitePrism::Page

    section :nav, Sections::NavBar, '#navbar'

    set_url '/customers'

    element :name, 'input[name=name]'
    element :phone, 'input[name=phone]'
    element :email, 'input[name=email]'
    element :notes, 'textarea[name=note]'
    element :auth_info, 'input[type=checkbox]'
    element :search_field, 'input[name=search]'
    element :delete_link, '#delete-button'

    element :add_button, '#dataview-insert-button'
    element :save_button, '#form-submit-button'
    element :search_button, 'button[id*=search]'
    element :delete_confirm, 'button[data-bb-handler=success]'

    elements :view, '#dataview-table tbody tr'

    element :modal_body, '.modal-body'
    element :remove_button_yes, '.modal-content button[class*=danger]'

    def save(customer)
        self.add_button.click
        self.name.set customer.name
        self.phone.set customer.phone
        self.email.set customer.email
        self.notes.set customer.notes
        self.auth_info.click if customer.auth_info == true
        self.save_button.click
    end

end