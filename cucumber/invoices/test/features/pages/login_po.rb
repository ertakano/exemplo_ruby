class LoginPage <SitePrism::Page

    set_url '/login'
    element :email, '#email'
    element :password, 'input[placeholder$=senha]'
    element :sign_in, '.login-button'
    element :msg_errors, '#login-errors'

    def do_login(usr, pwd)
        self.email.set usr
        self.password.set pwd
        self.sign_in.click
    end

    def do_login_vazio
        self.email.set ''
        self.password.set ''
        self.sign_in.click
    end

end